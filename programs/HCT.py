# Highest common taxa
from Bio import SeqIO
import argparse,argcomplete

################################################################################
# Input arguments parsing

parser = argparse.ArgumentParser(description='# This program selects taxonomic groups from the Silva database file.',
 epilog="Output of this program is in a pseudo .blast format. The file has a correct form, but the values such as sequence length' and 'percent of identity to the query' are not valid since those can vary for each alignment taken in account.")

parser.add_argument('-i','--in', dest='IN', help='Input file in a .blast format.')
parser.add_argument('-o','--out', dest='OUT', help='Output table.')
parser.add_argument('-d','--database', dest='DAT', help='Silva database with appropriate codes.')
parser.add_argument('-t','--treshold', dest='TRES', default=0.005, help='An accepted treshold of errors (outliers) in the input database (default = 0.005). Values between 0 and 1 are expected.')

argcomplete.autocomplete(parser)

args = parser.parse_args()

################################################################################
# Functions defined for this program

# Function translating codes into actual taxonomic assignments
def taxTranslate(codeList, tax_dictionary):
    taxList = []
    for entry in codeList:
        taxonomy = tax_dictionary[entry]
        taxList.append(taxonomy)
    return(taxList)

def HCT(taxList):
    highestCT = []
    taxListSplit = []
    taxListLen = []
    # Splitting tax list into list of lists
    # Finding the shortest taxonomic assignment
    for entry in taxList:
        entry = entry.split(";")
        taxListLen.append(len(entry))
        taxListSplit.append(entry)

    # Looping over all the common taxonomic assignment levels
    # Stopping as soon as the level names differ
    levelNum = 0
    penalty = 0
    minTaxListLen = min(taxListLen)
    # Treshold calculation
    # Minimum treshold is 1 (no errors accepted) +
    treshold = 1 + len(taxListLen)*float(args.TRES)
    treshold = round(treshold)
    while levelNum < minTaxListLen:
        # Current level name of the first entry
        levelName = taxListSplit[0][levelNum]
        # Looping over the list of lists of taxonomic assignments
        for entry in taxListSplit:
            # If the level names differ, penalty is raised by one
            if entry[levelNum] != levelName:
                penalty += 1
                break
        # If the penalty is at least 1 and higher then the treshold, the cycle is broken out from
        if penalty > treshold:
            break
        # If flag is positive, the common entry is attached to the results list and the level is raised by one
        highestCT.append(levelName)
        levelNum += 1
    return(";".join(highestCT))

################################################################################
# Program routine

tax_dictionary = {}

# Creating a dictionary translating the codes into actual taxonomic assignments
for record in SeqIO.parse(args.DAT, "fasta"):
    taxon = record.description.split()[1]
    code = record.description.split()[0]
    tax_dictionary[code] = taxon

headerOld = "firstRun"

codeList = []
with open(args.IN) as f_in, open(args.OUT, 'a') as f_out:
    for line in f_in:
        line = line.split()
        header = line[0]
        code = line[1]
        # With every new header, the taxonomy list (tax_list) is cleaned
        if headerOld != header and headerOld != "firstRun":
            taxList = taxTranslate(codeList, tax_dictionary)
            highestCT = HCT(taxList)
            codeList = []
            sampleName = line[0]
            # Splitting the header to extract dereplicated size
            headerSplit = header.split(";")
            # Dereplicate size
            size = headerSplit[1][5:]
            # The rest of the header
            headerShort = headerSplit[0]
            # Version with hard coded sample name (for one sample)
            f_out.write('X' + '\t' + highestCT + '\t' + 'Sample1' + '\t' + sampleName + '\t' + size + '\n')
            # Version with sample names
            #f_out.write(highestCT + '\t' + sampleName + '\t' + size + '\n')
        headerOld = header
        codeList.append(code)
