from Bio import SeqIO
import argparse,argcomplete

################################################################################
# Input arguments parsing

parser = argparse.ArgumentParser(description='# This program selects taxonomic groups from the Silva database file.',
 epilog="Output of this program is in a pseudo .blast format. The file has a correct form, but the values such as sequence length' and 'percent of identity to the query' are not valid since those can vary for each alignment taken in account.")

parser.add_argument('-i','--in', dest='IN', help='Input file in a .fasta format.')
parser.add_argument('-o','--out', dest='OUT', help='Output file in a .fasta format.')
parser.add_argument('-b','--blast', dest='BLAST', help='Output of the aligner in .blast format.')

argcomplete.autocomplete(parser)

args = parser.parse_args()

################################################################################

nameList = []

with open(args.BLAST) as f_in:
    for line in f_in:
        line = line.split()
        name = line[0]
        nameList.append(name)
        break

output = []

with open(args.IN,"r") as f_in, open(args.OUT, "a") as f_out:
    for record in SeqIO.parse(f_in, 'fasta'):
        if record.id not in nameList:
            output.append(record)
            SeqIO.write(record,f_out,'fasta')
