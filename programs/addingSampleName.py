# Highest common taxa
from Bio import SeqIO
import argparse,argcomplete

################################################################################
# Input arguments parsing

parser = argparse.ArgumentParser(description='# This program adds sample names in the headers.')

parser.add_argument('-i','--in', dest='IN', help='Input file.')
parser.add_argument('-o','--out', dest='OUT', help='Output file in a pseudo .blast format.')
parser.add_argument('-n','--name', dest='NAME', help='Sample name.')

argcomplete.autocomplete(parser)

args = parser.parse_args()

################################################################################

with open(args.IN) as f_in, open(args.OUT, 'a') as f_out:
    for line in f_in:
        lineOut = [args.NAME + '\t' + line]
        f_out.write("".join(lineOut))
