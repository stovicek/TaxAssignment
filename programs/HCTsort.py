import argparse
import pandas as pd
import numpy as np

pd.__version__

################################################################################
# Input arguments parsing

parser = argparse.ArgumentParser(description='# Sorting the stuff.')

parser.add_argument('-i','--in', dest='IN', help='Input file in a pseudo .blast format.')
parser.add_argument('-o','--out', dest='OUT', help='Output file in a pseudo .blast format.')

args = parser.parse_args()

################################################################################

#names = ['Header', 'Size' ,'Taxonomy', 'Similiarity', 'Col5', 'Col6', 'Col7', 'Col8', 'Col9', 'Col10', 'Col11', 'Col12']

names = ['Taxonomy', 'SampleName', 'SequenceName', 'Size']

df = pd.read_csv(args.IN, sep = '\t', names = names, header = None)

df = df.sort_values('Taxonomy')

df = df.groupby(['Taxonomy', 'SampleName'])

dfA = df.agg(np.sum)

dfAu = dfA.unstack(fill_value = 0)


dfAu.to_csv(args.OUT, sep = '\t')
