from Bio.Seq import Seq
from Bio import SeqIO
from Bio.Alphabet import generic_dna
import argparse

parser = argparse.ArgumentParser(description='# This program selects taxonomic groups from the Silva database file.')

parser.add_argument('-i','--in', dest='IN', help='Input database file in fasta format.')
parser.add_argument('-o','--out', dest='OUT', help='Output file in fasta format.')

args = parser.parse_args()

################################################################################

with open(args.IN, 'r') as f_in, open(args.OUT, 'a') as f_out:
    for record in SeqIO.parse(f_in, "fasta"):
        record = record.reverse_complement()
        SeqIO.write(record,f_out,'fasta')
