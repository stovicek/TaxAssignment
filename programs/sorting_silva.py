from Bio import SeqIO
import argparse,argcomplete

parser = argparse.ArgumentParser(description='# This program selects taxonomic groups from the Silva database file.')

parser.add_argument('-i','--in', dest='IN', type = argparse.FileType('r'), help='Input database in .fasta format.')
parser.add_argument('-o','--out', dest='OUT', help='Output file name.')
parser.add_argument('-t','--taxon', dest='TAX', help='Taxonomic category (Bacteria, Eukaryota or Archaea).')

argcomplete.autocomplete(parser)

args = parser.parse_args()

output = []


for record in SeqIO.parse(args.IN, "fasta"):
    taxon = record.description.split()[1]
    taxon = taxon.split(";")
    if taxon[0] == args.TAX:
        output.append(record)

SeqIO.write(output,args.OUT,'fasta')
