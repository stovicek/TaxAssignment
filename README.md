# Taxonomy assignment of assembled and non-assembled reads

This project is focusing on taxonomic and funtional classification of both assembled and non-assmbled reads after preliminary sorting with the **Infernal** program package.

### The databases utilised for taxonomic and functional classification

1. Taxonomic classification
 - [Silva v123](/Silva/summary.md)
2. Functional classification

### Following is an overview or applied methods:

1. [Vsearch](Vsearch/summary.md) - the open source version of Usearch available [here](https://github.com/torognes/vsearch).
2. Utax - Part of the Usearch package ([here](http://www.drive5.com/usearch/)) v8.1.1861.
