# Preparation of the Silva database

In order to speed up the classification process, the Silva database was split into categories (_Eukaryotes, Bacteria_ and _Archaea_) using [custom python script](../programs/sorting_silva.py).

```Shell
python3.5 -i SILVA_123_SSURef_Nr99_tax_silva.fasta -o eukaryota -t Eukaryota
```
