# Progress check board

Eukaryotes_SSU

- [x] R1
- [ ] R2
- [ ] R3
- [ ] R4

Eukaryotes_LSU

- [ ] R1
- [ ] R2
- [ ] R3
- [ ] R4

Bacteria_SSU

- [ ] R1
- [ ] R2
- [ ] R3
- [ ] R4

Bacteria_LSU

- [ ] R1
- [ ] R2
- [ ] R3
- [ ] R4

Archaea_SSU

- [ ] R1
- [ ] R2
- [ ] R3
- [ ] R4

Archaea_LSU

- [ ] R1
- [ ] R2
- [ ] R3
- [ ] R4
