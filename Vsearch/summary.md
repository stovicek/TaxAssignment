# Taxonomic assignment with the Vsearch package

 [Vsearch](https://github.com/torognes/vsearch) package is an open source alternative to the Usearch package.

### Workflow

1. Dereplication
 - This steps merges identical sequences thus reducing the computation effort required to reach the solution.
 - [Details of each run](dereplication/log.md).
 - [Project progress](dereplication/progress.md).

 ```Shell
 vsearch --derep_fulllength eukarya_r1.fastq --output eukarya_R1_derep.fastq --uc R1_derep.uc --sizeout
 ```

2. Sequence alignment against a databases
  - Sequences are classified with the [Silva database fragment](../Silva/summary.md) resenting the appropriate group (_Bacteria, Archaea_ or _Eukaryotes_).
  - [Details of each run](alignment/log.md).
  - [Project progress](alignment/progress.md).

```Shell
vsearch --usearch_global R2_derep.fasta -db eukaryota.fasta --alnout Vsearch/R2_99.align --blast6out Vsearch/R2_99.blast --log Vsearch/
R2_99.log --maxaccepts 0 --maxrejects 0 --id 0.99 --strand both --gapopen 100QL/20QI/100QR/100TL/20TI/100TR
```

3. Determining the highest common taxonomic group
  - Sequences resulting from conserved sites can yield multiple assignment results. In order to correctly assign the taxonomy the custom **highest common taxa** method was implemented. This method attempts to search for the highest taxonomic group that is common to all of the assignments and considers this the correct answer. The method was performed with a [custom python script](../programs/HCT.py).
  - The Silva database has many potentially wrongly assigned sequences. In order to deal with this a treshold based method was implemented, ignoring all 'wrong' entries that fall below the limit. The default value is 0.5 % of allowed 'outliers'.  

  ```Shell
python HCT.py -d eukaryota.fasta -input R1_99.blast -output R1_99.blast.HTC -treshold 0.005

python TaxAssignment/programs/HCTsort.py -i R1_99.blast.HTC -o R1_99.blast.HTC.sorted
  ```
