# The log of the dereplication step for all the samples

## Eukarya_r1

vsearch --derep_fulllength eukarya_r1.fastq --output eukarya_R1_derep_test.fastq --uc R1_derep_test.uc --log=log --sizeout
vsearch v1.10.1_linux_x86_64, 141.9GB RAM, 24 cores
https://github.com/torognes/vsearch

Reading file eukarya_r1.fastq 100%  
41735763 nt in 474 789 seqs, min 52, max 88, avg 88
Dereplicating 100%  
Sorting 100%
46361 unique sequences, avg cluster 10.2, median 1, max 1532
Writing output file 100%  
Writing uc file, first part 100%  
Writing uc file, second part 100%

vsearch --derep_fulllength R2_trimmed3.fasta --output taxAss/R2_derep.fasta --uc taxAss/R2_derep.uc
vsearch v1.10.1_linux_x86_64, 141.9GB RAM, 24 cores
https://github.com/torognes/vsearch

Reading file R2_trimmed3.fasta 100%
295596353 nt in 3 385 053 seqs, min 51, max 88, avg 87

Dereplicating 100%
Sorting 100%
558809 unique sequences, avg cluster 6.1, median 1, max 4103
Writing output file 100%
Writing uc file, first part 100%
Writing uc file, second part 100%

vsearch --derep_fulllength R3_trimmed3.fasta --output taxAss/R3_derep.fasta --uc taxAss/R3_derep.uc
vsearch v1.10.1_linux_x86_64, 141.9GB RAM, 24 cores
https://github.com/torognes/vsearch

Reading file R3_trimmed3.fasta 100%
351041866 nt in 4 009 803 seqs, min 51, max 88, avg 88
Dereplicating 100%
Sorting 100%
604718 unique sequences, avg cluster 6.6, median 1, max 923
Writing output file 100%
Writing uc file, first part 100%
Writing uc file, second part 100%

vsearch --derep_fulllength R4_trimmed4.fasta --output taxAss/R4_derep.fasta --uc taxAss/R4_derep.uc
vsearch v1.10.1_linux_x86_64, 141.9GB RAM, 24 cores
https://github.com/torognes/vsearch

Reading file R4_trimmed4.fasta 100%
101002419 nt in 1 156 483 seqs, min 51, max 88, avg 87
Dereplicating 100%
Sorting 100%
68388 unique sequences, avg cluster 16.9, median 1, max 857
Writing output file 100%
Writing uc file, first part 100%
Writing uc file, second part 100%
